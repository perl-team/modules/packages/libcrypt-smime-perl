libcrypt-smime-perl (0.30-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.30.

 -- gregor herrmann <gregoa@debian.org>  Sun, 31 Mar 2024 00:50:25 +0100

libcrypt-smime-perl (0.29-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.29.
    Closes: #1045585
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Tue, 05 Mar 2024 01:12:10 +0100

libcrypt-smime-perl (0.28-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.28.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 Oct 2021 16:55:40 +0200

libcrypt-smime-perl (0.27-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.27.
  * Drop test dependency on libtest-dependencies-perl.

 -- gregor herrmann <gregoa@debian.org>  Fri, 18 Sep 2020 21:25:01 +0200

libcrypt-smime-perl (0.26-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.
  * Replace '--with perl_openssl' in debian/rules with a build dependency
    on 'dh-sequence-perl-openssl'.

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Import upstream version 0.26.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Fri, 07 Aug 2020 04:23:15 +0200

libcrypt-smime-perl (0.25-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * New upstream release.
  * Declare compliance with Debian Policy 4.1.4.

 -- gregor herrmann <gregoa@debian.org>  Fri, 11 May 2018 16:28:44 +0200

libcrypt-smime-perl (0.21-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Declare compliance with Debian Policy 4.1.3.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Fri, 19 Jan 2018 19:36:53 +0100

libcrypt-smime-perl (0.19-2) unstable; urgency=medium

  * Team upload.
  * Dynamically generate a dependency on perl-openssl-abi-x.x
    to ensure compatibility with other OpenSSL-related Perl modules.
    (See #848113)

 -- Niko Tyni <ntyni@debian.org>  Tue, 03 Jan 2017 14:22:48 +0200

libcrypt-smime-perl (0.19-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.19

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 21 Dec 2016 07:21:22 +0100

libcrypt-smime-perl (0.18-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    Fixes "FTBFS with openssl 1.1.0". (Closes: #828389)
  * Enable autopkgtest use.t test by adding
    debian/tests/pkg-perl/use-name.

 -- gregor herrmann <gregoa@debian.org>  Thu, 27 Oct 2016 22:55:24 +0200

libcrypt-smime-perl (0.17-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 0.17
  * Drop unneeded alternative Build-Depends for ExtUtils::Constant.
    The required version is already satisfied in oldstable by perl itself,
    thus we can drop the alternative Build-Depends on perl (>= 5.13.7) or
    libextutils-constant-perl (>= 0.23).
  * Declare compliance with Debian policy 3.9.8
  * debian/rules: Build enabling all hardening flags

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 14 Aug 2016 20:08:04 +0200

libcrypt-smime-perl (0.16-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 0.16
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Add a build-dependency on ExtUtils::Constant 0.23
  * Update years of packaging copyright
  * Declare compliance with Debian Policy 3.9.6
  * Add debian and quilt directories to MANIFEST.SKIP for manifest.t
  * Mark package autopkgtest-able

 -- Florian Schlichting <fsfs@debian.org>  Tue, 06 Oct 2015 22:03:42 +0200

libcrypt-smime-perl (0.15-1) unstable; urgency=medium

  * Team upload.
  * Imported Upstream version 0.15

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 14 Aug 2014 22:27:17 +0200

libcrypt-smime-perl (0.14-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Mon, 04 Aug 2014 17:15:22 +0200

libcrypt-smime-perl (0.13-1) unstable; urgency=low

  * Team upload.

  [ Harlan Lieberman-Berg ]
  * New upstream version 0.12.
  * Remove patches applied upstream.
  * Add new dependency.

  [ gregor herrmann ]
  * New upstream release 0.13.
  * Update years of upstream copyright.
  * Add build dependency on libextutils-cchecker-perl.
  * Add more build dependencies to active more tests.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 May 2014 16:20:51 +0200

libcrypt-smime-perl (0.10-2) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Correct Vcs-Browser and Vcs-Git URLs.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.
  * Use debhelper 9.20120312 to get default hardening flags.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Fri, 11 Apr 2014 23:45:10 +0200

libcrypt-smime-perl (0.10-1) unstable; urgency=low

  * Initial Release. (Closes: #627914)

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Tue, 05 Jul 2011 00:09:03 +0000
